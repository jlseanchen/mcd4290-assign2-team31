// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];

//Feature 1
 class Run{
    constructor(startLoc,desLoc,arrayLoc,sDate,cDate,timeFinal,timeInitial,lastCurrentLocation,avgSpeed){
        this._startLocation=startLoc;
        this._destinationLocation=desLoc;
        this._arrayLocation=arrayLoc;
        this._startTime=sDate;
        this._completeTime=cDate;
        this._timeFinal=timeFinal;
        this._timeInitial=timeInitial;
        this._lastCurrentLocation=lastCurrentLocation;
        this._avgSpeed=avgSpeed;
    }
    get startLocation(){
        return this._startLocation
    }
    get destinationLocation(){
        return this._destinationLocation
    }
    get arrayLocation(){
        return this._arrayLocation
    }
    get startTime(){
        return this._startDate
    }
    get completeTime(){
        return this._completeDate
    }
    get runName(){
        return this._runName
    }
     get timeFinal(){
        return this._timeFinal
    }
     get timeInitial(){
        return this._timeInitial
    }
     get lastCurrentLocation(){
        return this._lastCurrentLocation
    }
     get avgSpeed(){
        return this._avgSpeed
    }
    set startLocation(newStartLocation){
        this._startLocation=newStartLocation
    }
    set destinationLocation(newDestinationLocation){
        this._destinationLocation=newDestinationLocation
    }
    set arrayLocation(newArrayLocation){
        this._arrayLocation=newArrayLocation
    }
    set startTime(newStartDate){
        this._startTime=newStartDate
    }
    set completeTime(newCompleteDate){
        this._completeTime=newCompleteDate
    }
    set runName(newRunName){
        this._runName = newRunName
    }
     set timeInitial(newTimeInitial){
        this._timeInitial = newTimeInitial
    }
     set timeFinal(newTimeFinal){
        this._timeFinal = newTimeFinal
    }
     set lastCurrentLocation(newLastCurrentLocation){
        this._lastCurrentLocation = newLastCurrentLocation
    }
     set avgSpeed(newAvgSpeed){
        this._avgSpeed = newAvgSpeed
    }
     distanceTravelled(){
         var diffLat = this._startLocation[0] - this._lastCurrentLocation[0];
         var diffLong = this._startLocation[1] - this._lastCurrentLocation[1];
         var difference = Math.sqrt(Math.pow(diffLat,2)+Math.pow(diffLong,2))
         var distanceStartToCurrent = difference * 150/0.001
         return distanceStartToCurrent
     }
     duration(){
         var minutes=this._timeFinal[0]-this._timeInitial[0]
         var seconds=this._timeFinal[1]-this._timeInitial[1]
         var durationOfRun=minutes+" mins"+" "+seconds+" secs"
         return durationOfRun
     }
     
     initialize(objectPDO){
         this._startLocation = objectPDO._startLocation
         this._duration = objectPDO._duration
         this._distance = objectPDO._distance
         this._destinationLocation = objectPDO._destinationLocation
         this._arrayLocation = objectPDO._arrayLocation
         this._startTime = objectPDO._startTime
         this._completeTime = objectPDO._completeTime
         this._runName = objectPDO._runName
         this._lastCurrentLocation = objectPDO._lastCurrentLocation
         this._avgSpeed = objectPDO._avgSpeed
         this._timeFinal = objectPDO._timeFinal
         this._timeInitial = objectPDO._timeInitial
         
     }
}
