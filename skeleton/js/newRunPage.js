// Code for the Measure Run page.
var Index = localStorage.getItem(APP_PREFIX + "-selectedRun");
var reattemptRunCheck = localStorage.getItem('sign')
//variable declaration             
var   currentLat,id, currentLong, currentPos,currentAcc,realAccuracy,startLoc,initialPosition = [0,0]
var marker=[]
var newCoordinate=[0,0]
var marker1=0
var diffLat, diffLong, difference,finalHour,finalMinute,finalSecond,timer,distanceStartToCurrent, timeDiff,distance,startMinute,startSecond,startHour,startDay,startMonth,startYear,startTime,timer,min,sec, RunInstance, stopTime,finalDay,finalMonth,finalYear,timeInitial,timeFinal
var runObject, reattemptRunInstance, reattemptRunInitialLocation,distanceDynamicTimer,reattemptDistance 


   document.getElementById("begin_run").disabled=true;
    document.getElementById("reattempt").disabled=true;
//function to check if the watchposition is successfull
function success(pos) { 
    var crd = pos.coords;
    currentLat = crd.latitude // obtain latitude
    currentLong = crd.longitude //obtain langitude
    currentAcc = crd.accuracy // obtain accuracy
    currentPos = [currentLong, currentLat] // put the latittude and langitude into an array
    document.getElementById("reattempt").style.left = "50px" 
    document.getElementById("deleteMarker").style.left = "20px"
    document.getElementById("stop_run").style.left = "82px"
}

//function to show error message if the ogeolocation function is not working
function error(err) { 
  console.warn('ERROR(' + err.code + '): ' + err.message);
}

// option
options = { 
  enableHighAccuracy: true,
  timeout: 10000,
  maximumAge: 0
};

id = navigator.geolocation.watchPosition(success,error,options);

document.getElementById("reattempt").disabled=true;


if(reattemptRunCheck == 2){
    alert('You are reattempting a run, please press reattempt run')
    document.getElementById("myBtn").disabled = true;
    document.getElementById("getAccuracy").disabled = true;
    document.getElementById("reattempt").disabled=false;
    var retrievedData = localStorage.getItem("RUN1TRIAL")
    var runObject = JSON.parse(retrievedData)
    reattemptRunInstance = new Run();
    reattemptRunInstance.initialize(runObject[Index])
    
             marker1 = new mapboxgl.Marker({color: 'red'})
        .setLngLat(reattemptRunInstance.startLocation)
        .addTo(map);
        
    
    
var popup1 = new mapboxgl.Popup({offset: 20 })
    .setLngLat(reattemptRunInstance.startLocation)
    .setText('Starting Location')
    .addTo(map);


var popup2 = new mapboxgl.Popup({offset: 20 })
    .setLngLat(reattemptRunInstance.lastCurrentLocation)
    .setText('Ending Location')
    .addTo(map);

         marker2 = new mapboxgl.Marker()
        marker2.setLngLat(reattemptRunInstance.lastCurrentLocation);
        marker2.addTo(map);

        map.panTo(reattemptRunInstance.startLocation)
    
 }

//function for the get accuracy button
function getAccuracy(){
var answerRef
answerRef = document.getElementById("realAccuracy");
realAccuracy = currentAcc;
startLoc = currentPos;
answerRef.innerText = realAccuracy;
randomDestinationButton()
enableBtn()
return startLoc
}

//Feature 3

//function to disable the random destination button if accuracy still above 20
function randomDestinationButton(){
    if (realAccuracy<=20){
        document.getElementById("myBtn").disabled = false;
    }
    else{
        document.getElementById("myBtn").disabled = true;
    }
}

//call randomDestination function
randomDestinationButton()  

//function to delete marker
function deleteMarker(){
    for(var i=marker.length-1;i>=0;i--){
        marker[i].remove()
    }
    document.getElementById("myBtn").disabled = false;
    document.getElementById("begin_run").disabled=true;
    document.getElementById("deleteMarker").disabled=true;
    clearInterval(distanceDynamicTimer)
    var disRef = document.getElementById("distanceRemaining");
    disRef.innerText = 0 +" m"
    
}

//enable delete marker button
document.getElementById("deleteMarker").disabled=true;

//Function that receive the current position and return the random location between 60-150m from current position.
function randomDestination(startLoc){
//disable random destination button
    document.getElementById("myBtn").disabled = true;
    document.getElementById("deleteMarker").disabled=false;
    
//local variable declaration
    let angle=Math.random()*2*Math.PI
    let distance=Math.random()*90+60
    let xDistance=distance*Math.cos(angle)
    let yDistance=distance*Math.sin(angle)
    let xCoordinate=0.001/150*xDistance
    let yCoordinate=0.001/150*yDistance
    if(angle<=Math.pi*2){
        newCoordinate[0]=startLoc[0]+xCoordinate
        newCoordinate[1]=startLoc[1]-yCoordinate
    }
    else if(angle<=Math.pi){
        newCoordinate[0]=startLoc[0]-xCoordinate
        newCoordinate[1]=startLoc[1]+yCoordinate
    }
    else if(angle<=Math.pi*3/2){
        newCoordinate[0]=startLoc[0]-xCoordinate
        newCoordinate[1]=startLoc[1]-yCoordinate
    }
    else{
        newCoordinate[0]=startLoc[0]+xCoordinate
        newCoordinate[1]=startLoc[1]+yCoordinate
    }
//set pin marker of the random destination to the map
         marker1 = new mapboxgl.Marker()
        .setLngLat(newCoordinate)
        .addTo(map);
//push to an array for delete function
    marker.push(marker1)
//call enableBtn to enable the set random point button
    enableBtn()
//Record the intial position
    initialPosition=startLoc
//timer for dynamic distance
distanceDynamicTimer=setInterval(distanceDynamic,1000)
 //find the distance between current position and the final position
    function distanceDynamic(){
    diffLat = newCoordinate[1] - currentLat;
    diffLong = newCoordinate[0] - currentLong;
    difference = Math.sqrt(Math.pow(diffLat,2)+Math.pow(diffLong,2))
    distance = difference * 150/0.001
//find the distance between the initial position to current position
    diffLat = currentLat - initialPosition[1];
    diffLong = currentLong - initialPosition[0];
    difference = Math.sqrt(Math.pow(diffLat,2)+Math.pow(diffLong,2))
    distanceStartToCurrent = difference * 150/0.001
//print distance travelled and distance remaining to HTML
    var currentDistanceref = document.getElementById("currentDis");
    currentDistanceref.innerText = distanceStartToCurrent.toFixed(2) +' m'
    var disRef = document.getElementById("distanceRemaining"); 
    disRef.innerText = distance.toFixed(2)+' m';
        return distanceStartToCurrent
    }
    return newCoordinate
}

//Feature4

//function to disable the begin run button if the user has not set a random location
function enableBtn()
{
   if(newCoordinate[0]===0){
      document.getElementById("begin_run").disabled=true;
      }
    else{
      document.getElementById("begin_run").disabled=false;
    }
}

//call enableBtn function


//feature 5
 
//function for the beginRun button
function beginRun(){
//disable delete marker button
    document.getElementById("deleteMarker").disabled=true;
//set the inital time
	time = new Date();
    startDay=time.getDay()+8
    startMonth=time.getMonth()+1
    startYear=time.getFullYear()
    startHour=time.getHours()
	startMinute = time.getMinutes()
	startSecond = time.getSeconds()
    timeInitial=[startMinute,startSecond]
    startTime=startDay + "/" + startMonth + "/" + startYear + " " + startHour + ":" + startMinute + ":" + startSecond
    sec = 0;
    min = 0;
    timer = setInterval(stopwatch,1000);
//disable random destination button 
    document.getElementById("myBtn").disabled = true;
//function for the stopwatch
    function stopwatch() {
 sec++;
 if (sec === 60) {
  sec = 0
  min++
 }
        //disable begin run and enable stop run button
        document.getElementById("stop_run").disabled=false;
        document.getElementById("begin_run").disabled=true;

        RunInstance = new Run (initialPosition,0,0,startTime,0,0,timeInitial)
        document.getElementById('time').innerHTML =min + " " + " min " + sec + " " + "sec";
        return sec,min
        
    }
        localStorage.setItem("sign",1)
  }

//feature 5

//disable stop run button
document.getElementById("stop_run").disabled=true;
var finalDistance

//function for the Stop Run button
function stopRun(){
//stop distance timer
clearInterval(distanceDynamicTimer)
//disable stop run button
document.getElementById("stop_run").disabled=true;
//stop timer
    clearInterval(timer)
//take finish time
    time = new Date();
    finalHour = time.getHours()
	finalMinute = time.getMinutes()
	finalSecond = time.getSeconds()
    finalDay=time.getDay()+8
    finalMonth=time.getMonth()+1
    finalYear=time.getFullYear()
    timeFinal=[finalMinute,finalSecond]
    stopTime= finalDay + "/" + finalMonth + "/" + finalYear + " " + finalHour + ":" + finalMinute + ":" + finalSecond
//calculate average speed
    var averageSpeed=distanceStartToCurrent/(min*60+sec)
//put the information to an object using class
    RunInstance.lastCurrentLocation= currentPos
     RunInstance.destinationLocation = newCoordinate
     RunInstance.completeTime = stopTime
    RunInstance.timeFinal=timeFinal
    RunInstance._avgSpeed=averageSpeed
//Enable save run button
    document.getElementById("save_run").disabled=false;
}

//Disable save run button
document.getElementById("save_run").disabled=true;

//feature 6
var runListarray = []
var key = 'RUN1TRIAL'
var doc
function saveRun(){
            doc = prompt("Please Enter Run Name "); 
            if (doc != null) { 
            } 
    RunInstance.runName = doc
    localStorage.setItem("sign",1)
        if (typeof(Storage) !== "undefined")
    {
        var stringifyRunTnstance = JSON.stringify(RunInstance)
            if  (localStorage.getItem("RUN1TRIAL")== null){
            runListarray.push(RunInstance)
            localStorage.setItem(key, JSON.stringify(runListarray))
            }

        else{

            var rerunListarray = JSON.parse(localStorage.getItem("RUN1TRIAL"))
            rerunListarray.push(RunInstance)

            localStorage.setItem(key, JSON.stringify(rerunListarray))
        }
    }
    else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }
}

//feature 9

//function that will trigerred by reattempt run button
function reattempt (){
    reattemptRunInitialLocation = reattemptRunInstance.startLocation
    diffLat = reattemptRunInitialLocation[1] - currentLat;
    diffLong = reattemptRunInitialLocation[0] - currentLong;
    difference = Math.sqrt(Math.pow(diffLat,2)+Math.pow(diffLong,2))
    reattemptDistance = difference * 150/0.001
//check if the distance of user is close enoough to the initial location
    if (reattemptDistance<20){
        alert('you are close enough')
        document.getElementById("begin_run").disabled=false;
        document.getElementById("reattempt").disabled=true;
    }
    else{
        alert('proceed to the initial postion and press begin run')
    }
    
    localStorage.setItem("sign",1)
}