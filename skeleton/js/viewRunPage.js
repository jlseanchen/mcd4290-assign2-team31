// Code for the View Run page.
//gwt index
var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun");
if (runIndex !== null)
{
    var runNames = [ "Run A", "Run B" ];
    document.getElementById("headerBarTitle").textContent = runNames[runIndex];
}


var marker1=0
var marker2=0
var runInstance
var AnswerRef

var retrievedIndex = localStorage.getItem(APP_PREFIX + "-selectedRun") //getting index from local storage
var retrievedData = localStorage.getItem("RUN1TRIAL") // getting the array of past runs
var runObject = JSON.parse(retrievedData) //parse the array of parse run
var Index = JSON.parse(retrievedIndex) // parse te retrieved Index

        runInstance = new Run();//creating new object
        runInstance.initialize(runObject[Index])// initializing desired runInstance to obtain method from Run class


//add marker
         marker1 = new mapboxgl.Marker({color: 'red'})
        .setLngLat(runInstance.startLocation)
        .addTo(map);
//set marker in the starting LOcation       
var popup1 = new mapboxgl.Popup({offset: 20 })
    .setLngLat(runInstance.startLocation)
    .setText('Starting Location')
    .addTo(map);
//set marker in the last current position
var popup2 = new mapboxgl.Popup({offset: 20 })
    .setLngLat(runInstance.lastCurrentLocation)
    .setText('Ending Location')
    .addTo(map);

//set marker
         marker2 = new mapboxgl.Marker()
        marker2.setLngLat(runInstance.lastCurrentLocation);
        marker2.addTo(map);

        map.panTo(runInstance.startLocation)

//create a line from start to final position
                map.on('load', function () {
 
map.addLayer({
"id": "route",
"type": "line",
"source": {
"type": "geojson",
"data": {
"type": "Feature",
"properties": {},
"geometry": {
"type": "LineString",
"coordinates": [runInstance.lastCurrentLocation,runInstance.startLocation
]
}
}
},
"layout": {
"line-join": "round",
"line-cap": "round"
},
"paint": {
"line-color": "#888",
"line-width": 8
}
});
});
let runName = runInstance.runName

answerRef = document.getElementById("headerBarTitle");

answerRef.innerText = runName; //setting header title

localStorage.setItem("sign",'1')

// displaying the needed information
var distanceRef,speedRef,durationRef
distanceRef = document.getElementById("distance");
distanceRef.innerText = runInstance.distanceTravelled();

speedRef = document.getElementById("speed");
speedRef.innerText = runInstance.avgSpeed;

durationRef = document.getElementById("duration");
durationRef.innerText = runInstance.duration();


//function to indicate it is a reattempt by storing a variable in sign local storage
function reattemptRun(){
    localStorage.setItem("sign",2)
}

//function to delete run
function deleteRun(){
    runObject.splice(Index,1)
    localStorage.setItem('RUN1TRIAL',JSON.stringify(runObject))
    location.href = 'index.html'
}


