// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.
//declaring variable
let runListElement = document.getElementById('runlist');
var marker1=0
var marker2=0
var runInstance
var retrievedData = localStorage.getItem("RUN1TRIAL") // get the pas run array from the local storage
var runObject = JSON.parse(retrievedData) // parse the array of past runs

        runInstance = new Run(); //creating a new object
        runInstance.initialize(runObject)// initializing runInstance to obtain method from Run class


//viewrun function to store runIndex in the local storage and redirecting it to the view run
function viewRun(runIndex)
{
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem(APP_PREFIX + "-selectedRun", runIndex);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}




    let listHTML = "";


    let indexing
    
    //       create HTML list items for each route, as below.
    for(let i = 0; i<runObject.length ; i++){
    runInstance.initialize(runObject[i])
    let runName = runInstance.runName
    let startTime = runInstance._startTime
    let completeTime = runInstance._completeTime
    
            // HTML format of list item is:
    listHTML += "<tr> <td onmousedown=\"viewRun("+i+")\" class=\"full-width mdl-data-table__cell--non-numeric\">" + runName ;
    listHTML += "<div class=\"subtitle\">"+"StartTime " + startTime + ", FinalTime: " + completeTime +"</div></td></tr>";
            runListElement.innerHTML = listHTML;
        
        }
    

//set the indication for reattempt run
localStorage.setItem("sign",'1')
    


